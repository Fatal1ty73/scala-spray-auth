name := "Spray-Auth"

version := "1.0"

scalaVersion := "2.10.5"

resolvers ++= Seq(
  DefaultMavenRepository,
  "Github Maven2 Repo" at "https://raw.github.com/ysae/maven2/master",
  "sonatype.snapshots" at "http://oss.sonatype.org/content/repositories/snapshots",
  "sonatype.release" at "http://oss.sonatype.org/content/repositories/releases",
  "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
  "akka.repository" at "http://akka.io/repository",
  "repo2.maven.org" at "http://repo2.maven.org/maven2/",
  "repository.jboss.org" at
    "http://repository.jboss.org/nexus/content/groups/public/",
  "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
)

val sprayVersion = "1.3.3"
val akkaVersion  = "2.3.13"
libraryDependencies ++= Seq(
  "com.typesafe.akka"     % "akka-kernel_2.10"                % akkaVersion,
  "com.typesafe.akka"     % "akka-actor_2.10"                 % akkaVersion,
  "com.typesafe.akka"     % "akka-contrib_2.10"               % akkaVersion,
  "com.typesafe.akka"     % "akka-slf4j_2.10"                 % akkaVersion,
  "com.typesafe.akka"     %% "akka-slf4j"                     % akkaVersion,
  "com.typesafe.akka"     % "akka-testkit_2.10"               % akkaVersion  % "test",
  "io.spray"              %% "spray-can"                      % sprayVersion,
  "io.spray"              %% "spray-routing"                  % sprayVersion,
  "io.spray"              %% "spray-httpx"                    % sprayVersion,
  "io.spray"              %% "spray-util"                     % sprayVersion,
  "io.spray"              %% "spray-client"                   % sprayVersion,
  "org.slf4j"            % "slf4j-log4j12"                    % "1.7.12" % "runtime",
  "log4j"                % "log4j"                            % "1.2.17",
  "joda-time"              % "joda-time"                      % "2.8.2")

/** Shell */
shellPrompt := { state => System.getProperty("user.name") + "> " }

shellPrompt in ThisBuild := { state => Project.extract(state).currentRef.project + "> " }

/** Compilation */
javaOptions ++= Seq("-server", "-XX:+DoEscapeAnalysis", "-Xmx1024M", "-XX:MaxPermSize=256M", "-Xdebug", "-Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5006")

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8", "-feature")

maxErrors := 20

pollInterval := 1000

logBuffered := false

cancelable := true

/** Console
initialCommands in console := "import ru.dz.auth._"*/