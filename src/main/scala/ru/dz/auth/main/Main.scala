package ru.dz.auth.main


import akka.actor._
import akka.io.IO
import ru.dz.auth.api.ExampleService
import spray.can.Http

import scala.concurrent.ExecutionContext

object Main extends App {

  implicit val ec = ExecutionContext.global
  implicit val system = ActorSystem("Auth-Example")

  // the handler actor replies to incoming HttpRequests
  val actor = system.actorOf(Props[ExampleService], name = "auth-service")
  IO(Http) ! Http.Bind(actor, interface = "localhost", port = 8080)
}