package ru.dz.auth.secure

import ru.dz.auth.secure.cookie.CookieAuthenticator
import spray.routing.directives.AuthMagnet

import scala.concurrent.ExecutionContext

import scala.concurrent.Future

trait Secure {

  protected val sessionCache = WebAppSessionCache

  object SessionCookieAuthenticator {
    def apply()(implicit executor: ExecutionContext): CookieAuthenticator[SessionData] = {
      new CookieAuthenticator[SessionData] {
        def apply(cookie: Option[SessionCookie]) = Future.successful(
          // Return None for a missing, or invalid, session. Triggers a failure
          // with the authenticate() directive meaning this user/session couldn't
          // be validated. The response either redirects the user to the "login"
          // page or in the case of UI API type responses, returns a proper JSON
          // object indicating failure.
          cookie match {
            case Some(cookie1) => sessionCache.getSession(cookie1.value)
            case _ => None
          }
        )
      }
    }

  }
}