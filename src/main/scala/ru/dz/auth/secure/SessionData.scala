package ru.dz.auth.secure

case class SessionData(id: String, username: String) {
  override def toString = "(id=" + id + ", username=" + username + ")"
}