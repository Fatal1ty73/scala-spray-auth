package ru.dz.auth.secure

import java.util.concurrent.TimeUnit

import org.joda.time

import scala.concurrent.{ Promise, ExecutionContext, Future }

import spray.routing._
import spray.routing.authentication._

import spray.util._
import spray.http._
import spray.http.HttpHeaders._

package object cookie {
  type CookieAuthenticator[T] = Option[SessionCookie] => Future[Option[T]]
}

case class SessionCookie(token: String = "") {
  lazy val value = token
  override def toString = value
}

object SessionCookie {

  // The name of the "session" cookie sent down to the client,
  // usually a browser.
  private val sessionCookieName = "SESSION"
  private val cookieMaxAge = TimeUnit.HOURS.toSeconds(16) // 16 Hours

  def apply(cookie: HttpCookie): SessionCookie =
    if (sessionCookieName.equalsIgnoreCase(cookie.name)) {
      apply(cookie.value.replace("SESSION=", ""))
    } else  {
      apply()
    }

  def getSessionCookieHeader(content: String = "",
                             expires: Option[DateTime] = None,
                             maxAge: Option[Long] = Some(cookieMaxAge)): `Set-Cookie` = {
    `Set-Cookie`(HttpCookie(name = sessionCookieName,
      content = content,
      expires = expires,
      maxAge = maxAge,
      // Set "secure = true" if you want the "Secure" flag set on
      // outgoing cookies, meaning the cookie will only be accepted
      // over HTTPS (secure) connections.
      secure = false,
      httpOnly = true))
  }

  def getUnsetSessionCookieHeader = getSessionCookieHeader(maxAge = Some(0L))

}

class UserAuthenticator[T](val authenticator: cookie.CookieAuthenticator[T])
                          (implicit val ec: ExecutionContext) extends WebAppAuthenticator[T] {

  override def authenticate(cookies: Option[Seq[HttpCookie]], ctx: RequestContext) = {
    authenticator {
      cookies match {
        // NOTE: Only returns the _first_ matched session cookie from the
        // request.  If the request contains more than one session cookie
        // (hack attack?), headOption ensures that only the first one is ever
        // processed and we totally ignore the others.
        case Some(cookie) => cookie.map(SessionCookie(_)).headOption
        case _ => None
      }
    }
  }

}