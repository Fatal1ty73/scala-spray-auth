package ru.dz.auth.secure

import java.util.UUID._
import java.util.concurrent.TimeUnit

import com.google.common.cache.CacheBuilder


object WebAppSessionCache extends SessionCache[SessionData] // Singleton

// In memory session cache, backed by Google's CacheBuilder.
trait SessionCache[T] {

  private lazy val sessionCache = CacheBuilder.newBuilder()
    .expireAfterAccess(16, TimeUnit.HOURS)
    .asInstanceOf[CacheBuilder[String, T]]
    .build[String, T]()
    .asMap() // Concurrent map, fwiw

  def getSession(sessionId: String): Option[T] =
    Option(sessionCache.get(sessionId)) // Wrapped in Option() to handle null's

  def setSession(sessionId: String, data: T): Option[T] =
    Option(sessionCache.put(sessionId, data)) // Wrapped in Option() to handle null's

  def removeSession(sessionId: String): Option[T] =
    Option(sessionCache.remove(sessionId)) // Wrapped in Option() to handle null's

  def getRandomSessionId: String = {
    // Base-64 URL safe encoding (for the cookie value) and no chunking of
    // the encoded output (important).
//    encodeBase64URLSafe(randomUUID.toString)
   randomUUID.toString
  }

}