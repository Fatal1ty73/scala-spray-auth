package ru.dz.auth.api

import akka.actor.Actor
import akka.event.Logging
import ru.dz.auth.secure.SessionCookie._
import ru.dz.auth.secure._
import ru.dz.auth.secure.cookie.CookieAuthenticator
import spray.http.HttpHeaders._
import spray.http.MediaTypes._
import spray.http.StatusCodes._
import spray.http.{HttpResponse, MediaType}
import spray.routing._

class ExampleService extends Actor
with RouteConcatenation with HttpService with Secure {
  def actorRefFactory = context
  implicit def executionContext = actorRefFactory.dispatcher

  implicit val myRejectionHandler = RejectionHandler {
    case MissingSessionCookieRejection :: _ => complete(redirectToRoute("/logout"))
    case WebAppAuthenticationRejection :: _ => complete(redirectToRoute("/logout"))
  }

  def receive = runRoute(exampleRoutes)
  val logger = Logging(context.system,this)

  override def preStart() = {
    logger.debug("Starting REST")
  }

  protected def redirectToRoute(route: String): HttpResponse = {
    HttpResponse(status = Found, headers = List(Location(route)))
  }

  def render(uri: String, attributes: Map[String, Any] = Map.empty,
             mediaType: MediaType = `text/html`): Route = {
    respondWithMediaType(mediaType) {
      getFromResource(uri)
    }
  }
  protected def authenticator[U](authenticator: CookieAuthenticator[U] = SessionCookieAuthenticator()) =
    new UserAuthenticator[U](authenticator)

  val exampleRoutes = {

    //This doesn't need authentication, because we want it to
    //be a "grants all". In theory, if we still want to bring the
    //auth in scope (see below), we can write another trivial handler
    //which "grants all".
    //
    //Suppose you want to do something different for different roles:
    //you can create an authentication handler which simply brings in
    //scope the auth object, and then inside the complete put some
    //conditional code or whatever. It should also possible to use some
    //clever spray directive to avoid the nasty conditional branch, but
    //we are not aware of anything like that.
    path("home"|"") {
      get {
        logger.info("Home Page")
        authenticate(authenticator()) { session =>
          get {
//            logger.warning(session.toString)
            ctx => ctx.complete("yeah")
          }
        }
      }
    } ~
    path("login") {
      get {
        logger.info("Check login")
        render("web-app/index.html", Map("publicResourcePath" -> "localhost:8080"))
      } ~
        post {
          logger.debug("Check login")
          formFields("username" ?, "password" ?) {
            (username: Option[String], password: Option[String]) => {
              val session: Option[SessionData] = {
                if (password.getOrElse("") == "admin") {
                  Some(SessionData(id = sessionCache.getRandomSessionId,
                    username = username.getOrElse("")))
                } else {
                  None
                }
              }
              session match {
                case Some(session) => {
                  logger.info("Created session (login): " + session)
                  sessionCache.setSession(session.id, session)
                  respondWithHeader(getSessionCookieHeader(content = session.id)) {
                    _.complete(redirectToRoute("/home"))
                  }
                }
                case _ => {
                  respondWithHeader(getUnsetSessionCookieHeader) {
                    _.complete(redirectToRoute("/login?error=1"))
                  }
                }
              }
            }
          }
        }
    } ~
      path("logout") {
        (get | post | put | delete) {
          // Clobber session, then redirect to login page.  Sets a cookie
          // with an empty value ("") and sets its max-age to zero effectively
          // telling the client (usually a browser) to delete the session cookie.
          respondWithHeader(getUnsetSessionCookieHeader) {
            ctx => {
              for {
                `Cookie`(cookies) <- ctx.request.headers
                sessionCookies = cookies.map(SessionCookie(_))
                cookie <- sessionCookies
                sessionId = cookie.value
              } {
                // Remove the session from the internal session cache.
                sessionCache.removeSession(sessionId) match {
                  case Some(session: SessionData) => {
                    logger.info("Removed session (logout): " + session)
                  }
                  case _ => {
                    // Ignore, silently.
                  }
                }
              }
              ctx.complete(redirectToRoute("/login"))
            }
          }
        }
      } ~
      path("resource" / "all") {
        get {
          complete {
            "Morning, guest."
          }
        }
      }
  }
}